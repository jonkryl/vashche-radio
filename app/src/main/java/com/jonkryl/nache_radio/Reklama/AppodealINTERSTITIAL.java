package com.jonkryl.nache_radio.Reklama;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.RelativeLayout;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.native_ad.views.NativeAdViewAppWall;
import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.R;

/**
 * Created by jonkryl on 19.03.2018.
 */

public class AppodealINTERSTITIAL {
    private Context context;

    public AppodealINTERSTITIAL(Context context) {
        this.context = context;
    }

    public void init(Activity activity){
        Appodeal.initialize(activity, Constant.APPKEY, Appodeal.INTERSTITIAL, activity.getIntent().getBooleanExtra(Constant.CONSENT, false));
        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            @Override
            public void onInterstitialLoaded(boolean isPrecache) {
            }

            @Override
            public void onInterstitialFailedToLoad() {
            }

            @Override
            public void onInterstitialShown() {
            }

            @Override
            public void onInterstitialClicked() {
            }

            @Override
            public void onInterstitialClosed() {
                sendRestartPlayer();
            }

            @Override
            public void onInterstitialExpired() {

            }
        });
    }

    public void sendRestartPlayer() {
        Intent intent = new Intent();
        intent.setAction(Constant.ACTION_RESTART_PLAY);
        intent.putExtra(Constant.ACTION_RESTART_PLAY, 0);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    public void show(Activity activity){
        if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
            Appodeal.show(activity, Appodeal.INTERSTITIAL);
        }
    }

    public void destroy(){
        Appodeal.destroy(Appodeal.INTERSTITIAL);
    }
}

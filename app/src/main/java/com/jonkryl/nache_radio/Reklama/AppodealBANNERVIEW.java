package com.jonkryl.nache_radio.Reklama;

import android.app.Activity;

import com.appodeal.ads.Appodeal;
import com.jonkryl.nache_radio.Activity.RadioActivity;
import com.jonkryl.nache_radio.Const.Constant;

/**
 * Created by jonkryl on 19.03.2018.
 */

public class AppodealBANNERVIEW {

    public AppodealBANNERVIEW() {
    }

    public void init(Activity activity, int id) {
        Appodeal.setBannerViewId(id);
        Appodeal.initialize(activity, Constant.APPKEY, Appodeal.BANNER_VIEW, activity.getIntent().getBooleanExtra(Constant.CONSENT, false));
        Appodeal.show(activity, Appodeal.BANNER_VIEW);
    }

    public void destroy() {
        Appodeal.destroy(Appodeal.BANNER_VIEW);
    }
}

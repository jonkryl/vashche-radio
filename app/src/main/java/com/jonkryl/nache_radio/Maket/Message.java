package com.jonkryl.nache_radio.Maket;

import java.util.Date;

public class Message {
    private String txtMsg;
    private String userName;
    private String userPhotoUrl;
    private long timeMessage;
    private String userEmail;

    public Message(String txtMsg, String userName, String userPhotoUrl, String userEmail) {
        this.userName = userName;
        this.txtMsg = txtMsg;
        this.userPhotoUrl = userPhotoUrl;
        timeMessage = new Date().getTime();
        this.userEmail = userEmail;

    }

    public String getTxtMsg() {
        return txtMsg;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public long getTimeMessage() {
        return timeMessage;
    }

    public String getUserEmail() {
        return userEmail;
    }
}

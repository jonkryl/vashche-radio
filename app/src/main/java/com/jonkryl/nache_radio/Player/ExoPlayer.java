package com.jonkryl.nache_radio.Player;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.parsSongInfo.Network;
import com.jonkryl.nache_radio.parsSongInfo.SongData;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.AUDIO_SERVICE;

/**
 * Created by jonkryl on 05.03.2018.
 */

public class ExoPlayer {

    private Intent intent;
    private DataSource.Factory dataSourceFactory;
    private SimpleExoPlayer player;
    private Context context;
    private Network network = new Network();

    private Timer time;
    private boolean playing = false;
    public String streamAuthor;

    public ExoPlayer(Context context) {
//        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//        TrackSelection.Factory videoTrackSelectionFactory =
//                new AdaptiveTrackSelection.Factory(bandwidthMeter);
//        TrackSelector trackSelector =
//                new DefaultTrackSelector(videoTrackSelectionFactory);
//        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
//        dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "yourApplicationName"), (TransferListener<? super DataSource>) bandwidthMeter);
        player = ExoPlayerFactory.newSimpleInstance(context);
//        player.addListener(new Player.EventListener() {
//            @Override
//            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                for (int i = 0; i < trackGroups.length; i++) {
//                    TrackGroup trackGroup = trackGroups.get(i);
//                    for (int j = 0; j < trackGroup.length; j++) {
//                        Metadata trackMetadata = trackGroup.getFormat(j).metadata;
//                        if (trackMetadata != null) {
//                            sendString(Constant.ACTION_REFRESCH_AUTOR, trackMetadata.get(0).toString());
//                        }
//                    }
//                }
//            }
//        });
        dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "@string/app_name"));
        this.context = context;
    }

    class PlayerAsync extends AsyncTask<String, Void, Void> {


        @Override
        protected Void doInBackground(String... strings) {
            Uri stream = Uri.parse(strings[0]);
            MediaSource audioSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(stream);
            player.prepare(audioSource);

            return null;
        }

        @Override
        protected void onCancelled() {
            Log.d("MyLog", "taskstop");
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            Log.d("MyLog", "taskStart");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            sendBoolean(Constant.ACTION_REFRESCH_STATUS, false);
            super.onPostExecute(aVoid);
        }
    }

    public void startWGFMPlayer(String stream, int chanel) {
        Log.d("Mylog", "start_radio");
        AudioManager audioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(afChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            playing = true;
            new PlayerAsync().execute(stream);
            player.setPlayWhenReady(true);
            sendBoolean(Constant.ACTION_REFRESCH_INDICATOR, playing);
            int i = chanel - 1;
            if (i < 0) {
                i = 0;
            }
            startParceMetadata(Constant.STREAM_ID[i], stream);
        }
    }

    public void stopWGFMPlayer() {
        Log.d("Mylog", "stop_radio");
        playing = false;
        player.setPlayWhenReady(false);
        sendBoolean(Constant.ACTION_REFRESCH_INDICATOR, playing);
        sendString("");
        stopParceMetadata();
    }

    public void closeWGFMPlayer() {
        player.setPlayWhenReady(false);
        stopParceMetadata();
        playing = false;
        sendBoolean(Constant.ACTION_STOP_PLAYER, playing);
        player.release();
    }

    private void startParceMetadata(int streamId, String streamUrl) {
        time = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                network.getApiService().getSongDate(streamId, streamUrl, 0)
                        .enqueue(new Callback<SongData>() {
                            @Override
                            public void onResponse(@NonNull Call<SongData> call, @NonNull Response<SongData> response) {
                                if (response.body() != null) {
                                    sendString(response.body().getData().getArtist() + " - " + response.body().getData().getSong());
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<SongData> call, @NonNull Throwable t) {
                                Toast.makeText(context, "Ошибка " + t, Toast.LENGTH_LONG).show();
                            }
                        });
            }
        }, 1000, 2000);

    }

    private void stopParceMetadata() {
        if (time != null) {
            time.cancel();
        }
        time = null;
        streamAuthor = "";
        sendString(streamAuthor);
    }

    private void sendBoolean(String action, boolean howPlay) {
        intent = new Intent();
        intent.setAction(action);
        intent.putExtra(action, howPlay);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    private void sendString(String author) {
        intent = new Intent();
        intent.setAction(Constant.ACTION_REFRESCH_AUTOR);
        intent.putExtra(Constant.ACTION_REFRESCH_AUTOR, author);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);
    }

    private AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    player.setVolume(0.0f);
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    player.setVolume(0.5f);
                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                    player.setVolume(1.0f);
                    break;
            }
        }
    };


}


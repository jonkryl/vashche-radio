package com.jonkryl.nache_radio.Const;


/**
 * Created by jonkryl on 05.03.2018.
 */

public class Constant {
    public static final String CONSENT = "consent";

    public static final int[] STREAM_ID = {183, 5665, 183, 183,
            406108, 406107, 406109};

    public static final String PARSER_URL = "stations/last";

    public static final String NASHE = "http://nashe1.hostingradio.ru/nashe-128.mp3";
    public static final String NASHE_64 = "http://nashe1.hostingradio.ru/nashe-64.mp3";

    public static final String DVA_NOL = "http://nashe1.hostingradio.ru/nashe20-128.mp3";
    public static final String DVA_NOL_64 = "http://nashe1.hostingradio.ru/nashe20-64.mp3";

    public static final String SPB = "http://nasheekt.hostingradio.ru/nashespb128.mp3";
    public static final String SPB_64 = "http://nasheekt.hostingradio.ru/nashespb128.mp3";

    public static final String EKB = "http://nashe1.hostingradio.ru/nasheekt128.mp3";
    public static final String EKB_64 = "http://nashe1.hostingradio.ru/nasheekt128.mp3";

    public static final String CLASSIC = "http://nashe1.hostingradio.ru/nasheclassic.mp3";
    public static final String CLASSIC_64 = "http://nashe1.hostingradio.ru/nasheclassic.mp3";

    public static final String PUNK = "http://nashe1.hostingradio.ru/nashepunks.mp3";
    public static final String PUNK_64 = "http://nashe1.hostingradio.ru/nashepunks.mp3";

    public static final String POYU = "http://nashe1.hostingradio.ru/nashesongs.mp3";
    public static final String POYU_64 = "http://nashe1.hostingradio.ru/nashesongs.mp3";

    public static final String ACTION_PLAY_PAUSE = "NASHE_PLAY_AND_PAUSE";
    public static final String ACTION_STOP = "NASHE_ACTION_STOP";
    public static final String ACTION_STOP_PLAYER = "NASHE_ACTION_STOP_PLAYER";
    public static final String ACTION_CLICK_CHANNEL = "NASHE_CLICK_CHANNEL";
    public static final String ACTION_CLICK_GOOD_CHANNEL = "NASHE_CLICK_GOOD_CHANNEL";
    public static final String ACTION_RESTART_PLAY = "NASHE_RESTART_PLAY";

    public static final String ACTION_REFRESCH_STATUS = "NASHE_REFRESCH_STATUS";
    public static final String ACTION_REFRESCH_INDICATOR = "NASHE_REFRESCH_INDICATOR";
    public static final String ACTION_REFRESCH_AUTOR = "NASHE_REFRESCH_AUTOR";

    public static final String ACTION_GET_UPDATE = "NASHE_GET_UPDATE";
    public static final String ACTION_UPDATE_NOW_CHANNEL = "NASHE_UPDATE_NOW_CHANNEL";
    public static final String ACTION_UPDATE_GOOD_CHANNEL = "NASHE_UPDATE_GOOD_CHANNEL";

    public static final String CHAT_BROADCAST = "NASHE_CHAT_BROADCAST";

    public static final String ADMIN_1_EMAIL = "jonkryl@gmail.com";


    public static final String APPKEY = "e7d41a3bebd045e74d3ceaa80ac42ea18e2be9c2a38e36a3";

    public static final int NOTIFY_ID = 11;

    public static final String LETO = "https://firebasestorage.googleapis.com/v0/b/vasheradio-e7bab.appspot.com/o/leto.jpg?alt=media&token=0d52c2dc-806e-4ce2-94ce-1f11568364a9";
    public static final String OSEN = "https://firebasestorage.googleapis.com/v0/b/vasheradio-e7bab.appspot.com/o/osen.jpg?alt=media&token=e919a6f2-89ef-4a0a-9fff-64deac018fe0";
    public static final String VESNA = "https://firebasestorage.googleapis.com/v0/b/vasheradio-e7bab.appspot.com/o/vesna.jpg?alt=media&token=8faf4634-e714-4e0f-bf62-5c3f43114bf9";
    public static final String ZIMA = "https://firebasestorage.googleapis.com/v0/b/vasheradio-e7bab.appspot.com/o/zima.jpg?alt=media&token=2dd17ce2-a3fd-4daa-978d-f0039a719e64";
}

package com.jonkryl.nache_radio.Notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.jonkryl.nache_radio.Activity.RadioActivity;
import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.R;

/**
 * Created by jonkryl on 06.03.2018.
 */

public class PlayerNotification {
    private Context context;

    public PlayerNotification(Context context) {
        this.context = context;
    }

    public Notification getNotif(String playPause, String author, Boolean nowPlay) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, RadioActivity.class);
        PendingIntent pIntent1 = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentPlayPause = new Intent(Constant.ACTION_PLAY_PAUSE);
        intentPlayPause.putExtra(Constant.ACTION_PLAY_PAUSE, nowPlay);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intentPlayPause, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentStop = new Intent(Constant.ACTION_STOP);
        PendingIntent pendingIntentStop = PendingIntent.getBroadcast(context, 2, intentStop, PendingIntent.FLAG_UPDATE_CURRENT);
// Строим уведомление
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelId = context.getString(R.string.app_name);
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(channelId);
            notificationChannel.setSound(null, null);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            nm.createNotificationChannel(notificationChannel);
            Notification builder = new Notification.Builder(context, channelId)
                    .setContentTitle("Сейчас в эфире:")
                    .setContentText(author)
                    .setSmallIcon(R.drawable.ic_action_name)
                    .setContentIntent(pIntent1)
                    .addAction(0, "Stop", pendingIntentStop)
                    .addAction(0, playPause, pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .build();


            builder.flags = builder.flags | Notification.FLAG_ONGOING_EVENT;

            nm.notify(Constant.NOTIFY_ID, builder);
            return builder;
        } else {
            Notification builder = new Notification.Builder(context)
                    .setContentTitle("Сейчас в эфире:")
                    .setContentText(author)
                    .setSmallIcon(R.drawable.ic_action_name)
                    .setContentIntent(pIntent1)
                    .addAction(0, "Stop", pendingIntentStop)
                    .addAction(0, playPause, pendingIntent)
                    .setPriority(Notification.PRIORITY_MAX)
                    .build();


            builder.flags = builder.flags | Notification.FLAG_ONGOING_EVENT;

            nm.notify(Constant.NOTIFY_ID, builder);
            return builder;
        }


    }

}

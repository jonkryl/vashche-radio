package com.jonkryl.nache_radio.Activity;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.R;
import com.jonkryl.nache_radio.Reklama.AppodealBANNERVIEW;
import com.jonkryl.nache_radio.Service.PlayerService;
import com.jonkryl.nache_radio.parsBanner.BannerDate;
import com.jonkryl.nache_radio.parsBanner.Network;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jonkryl on 02.03.2018.
 */

public class RadioActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private Button btnMin;
    private Button btnMax;
    private Button btnPlayAndStop;

    private Button btnTrans;
    private Button btnRock;
    private Button btnDubler;
    private Button btnGlavnui;
    private Button btnClassic;
    private Button btnPunk;
    private Button btnPoyu;

    private String link;
    private String linkBanner;

    private ImageView imageDravable;
    private ImageView gifImageView;

    private TextView textAuthor;

    private ProgressBar progressBar;

    private boolean goodChannel = false;
    private boolean nowPlayGlobal = false;

    private int nowChannel;

    private Intent intent;
    private BroadcastReceiver receiverRadio;
    private AppodealBANNERVIEW appodealBANNERVIEW;

    private Random random = new Random();
    private Network network = new Network();

    private long lastPressedTime;
    private static final int PERIOD = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radio_activity);
        startService(new Intent(this, PlayerService.class));
        init();
        clickSet();
        registerReceiver();
        initNavigationMenu();
        getMonth();
        appodealBANNERVIEW.init(this, R.id.appodealBannerViewRadio);
    }

    private void getMonth() {
        Calendar calendar = Calendar.getInstance();
        int Month = calendar.get(Calendar.MONTH);

        if (Month == 11 || Month == 0 || Month == 1) {
            loadBackgraund(Constant.ZIMA, imageDravable);
        }
        if (Month == 2 || Month == 3 || Month == 4) {
            loadBackgraund(Constant.VESNA, imageDravable);
        }
        if (Month == 5 || Month == 6 || Month == 7) {
            loadBackgraund(Constant.LETO, imageDravable);
        }
        if (Month == 8 || Month == 9 || Month == 10) {
            loadBackgraund(Constant.OSEN, imageDravable);
        }
    }

    private void loadBackgraund(String url, ImageView imageView) {
        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.background)
                .into(imageView);
    }

    private void initNavigationMenu() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.chat);
        fab.setOnClickListener(view -> startActivity(new Intent("com.jonkryl.nache_radio.Activity.Chat")));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void init() {
        btnMin = findViewById(R.id.min);
        btnMax = findViewById(R.id.max);
        btnPlayAndStop = findViewById(R.id.playAndPause);

        btnTrans = findViewById(R.id.trans);
        btnRock = findViewById(R.id.rock);
        btnDubler = findViewById(R.id.dubler);
        btnGlavnui = findViewById(R.id.glavnui);
        btnClassic = findViewById(R.id.classik);
        btnPunk = findViewById(R.id.pank);
        btnPoyu = findViewById(R.id.poyu);

        textAuthor = findViewById(R.id.textAuthor);

        progressBar = findViewById(R.id.progressBar);

        imageDravable = findViewById(R.id.imageDravable);
        gifImageView = findViewById(R.id.bannerMe);

        appodealBANNERVIEW = new AppodealBANNERVIEW();
    }

    private void clickSet() {
        btnGlavnui.setOnClickListener(this);
        btnDubler.setOnClickListener(this);
        btnRock.setOnClickListener(this);
        btnTrans.setOnClickListener(this);
        btnPoyu.setOnClickListener(this);
        btnPunk.setOnClickListener(this);
        btnClassic.setOnClickListener(this);

        btnPlayAndStop.setOnClickListener(this);
        btnMax.setOnClickListener(this);
        btnMin.setOnClickListener(this);

        textAuthor.setSelected(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.min:
                if (goodChannel) {
                    goodChannel = false;
                    sendBoolean(Constant.ACTION_CLICK_GOOD_CHANNEL, goodChannel);
                }
                break;
            case R.id.max:
                if (!goodChannel) {
                    goodChannel = true;
                    sendBoolean(Constant.ACTION_CLICK_GOOD_CHANNEL, goodChannel);
                }
                break;
            case R.id.playAndPause:
                clickPlayAndSPause();
                break;
            case R.id.trans:
                clickChannel(4);
                break;
            case R.id.rock:
                clickChannel(3);
                break;
            case R.id.dubler:
                clickChannel(2);
                break;
            case R.id.glavnui:
                clickChannel(1);
                break;
            case R.id.classik:
                clickChannel(5);
                break;
            case R.id.pank:
                clickChannel(6);
                break;
            case R.id.poyu:
                clickChannel(7);
                break;
            default:
                break;
        }
    }

    private void clickPlayAndSPause() {
        if (nowChannel == 0) {
            nowChannel = 1;
        }

        if (nowPlayGlobal) {
            nowPlayGlobal = false;
        } else {
            nowPlayGlobal = true;
            progressBar.setVisibility(View.VISIBLE);
        }
        sendBoolean(Constant.ACTION_PLAY_PAUSE, nowPlayGlobal);
    }

    private void clickChannel(int id) {
        if (nowChannel != id) {
            clickChannelTrue(id);
        } else {
            if (!nowPlayGlobal) {
                clickChannelTrue(id);
            }
        }
    }

    private void clickChannelTrue(int id) {
        nowChannel = id;
        sendInt(id);
    }

    private void sendBoolean(String action, boolean goodStatus) {
        intent = new Intent();
        intent.setAction(action);
        intent.putExtra(action, goodStatus);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void sendInt(int id) {
        intent = new Intent();
        intent.setAction(Constant.ACTION_CLICK_CHANNEL);
        intent.putExtra(Constant.ACTION_CLICK_CHANNEL, id);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void registerReceiver() {
        receiverRadio = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constant.ACTION_REFRESCH_INDICATOR:
                        nowPlayGlobal = intent.getBooleanExtra(action, false);
                        returnPlayAndPause(nowPlayGlobal);
                        break;
                    case Constant.ACTION_REFRESCH_AUTOR:
                        textAuthor.setText(intent.getStringExtra(action));
                        break;
                    case Constant.ACTION_REFRESCH_STATUS:
                        returnProgressBarDownload(intent.getBooleanExtra(action, false));
                        break;
                    case Constant.ACTION_STOP_PLAYER:
                        stopApp(intent.getBooleanExtra(action, false));
                        break;
                    case Constant.ACTION_UPDATE_NOW_CHANNEL:
                        nowChannel = intent.getIntExtra(action, 0);
                        setFokusChannel(nowChannel, nowPlayGlobal);
                        break;
                    case Constant.ACTION_UPDATE_GOOD_CHANNEL:
                        goodChannel = intent.getBooleanExtra(action, false);
                        setFokusGoodChannel(goodChannel, nowPlayGlobal);
                        break;
                }
            }

        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFilt = new IntentFilter(Constant.ACTION_REFRESCH_INDICATOR);
        intFilt.addAction(Constant.ACTION_REFRESCH_AUTOR);
        intFilt.addAction(Constant.ACTION_REFRESCH_STATUS);
        intFilt.addAction(Constant.ACTION_STOP_PLAYER);
        intFilt.addAction(Constant.ACTION_UPDATE_NOW_CHANNEL);
        intFilt.addAction(Constant.ACTION_UPDATE_GOOD_CHANNEL);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(receiverRadio, intFilt);
    }

    private void stopApp(boolean booleanExtra) {
        returnPlayAndPause(booleanExtra);
        finish();
    }

    private void returnProgressBarDownload(boolean booleanExtra) {
        if (booleanExtra) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void returnPlayAndPause(boolean nowPlay) {
        if (nowPlay) {
            btnPlayAndStop.setText(R.string.Pause);
        } else {
            btnPlayAndStop.setText(R.string.Play);
        }
        nowPlayGlobal = nowPlay;
        setFokusChannel(nowChannel, nowPlayGlobal);
        setFokusGoodChannel(goodChannel, nowPlayGlobal);
    }

    @Override
    protected void onResume() {
//        getBanner();
        sendGetUpdate();
        appodealBANNERVIEW.init(this, R.id.appodealBannerViewRadio);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void sendGetUpdate() {
        intent = new Intent();
        intent.setAction(Constant.ACTION_GET_UPDATE);
        intent.putExtra(Constant.ACTION_GET_UPDATE, 0);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        if (!nowPlayGlobal) {
            sendBoolean(Constant.ACTION_STOP, false);
        }
        unregisterReceiver(receiverRadio);
        appodealBANNERVIEW.destroy();
        super.onDestroy();
    }

    private void setFokusChannel(int id, boolean b) {
        if (id == 0) {
            id = 1;
        }
        if (b) {
            switch (id) {
                case 1:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.fokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 2:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.fokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 3:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.fokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 4:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.fokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 5:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.fokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 6:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.fokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
                    break;
                case 7:
                    btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
                    btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
                    btnRock.setTextColor(getResources().getColor(R.color.noFokus));
                    btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
                    btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
                    btnPoyu.setTextColor(getResources().getColor(R.color.fokus));
                    break;
            }
        } else {
            btnGlavnui.setTextColor(getResources().getColor(R.color.noFokus));
            btnDubler.setTextColor(getResources().getColor(R.color.noFokus));
            btnRock.setTextColor(getResources().getColor(R.color.noFokus));
            btnTrans.setTextColor(getResources().getColor(R.color.noFokus));
            btnClassic.setTextColor(getResources().getColor(R.color.noFokus));
            btnPunk.setTextColor(getResources().getColor(R.color.noFokus));
            btnPoyu.setTextColor(getResources().getColor(R.color.noFokus));
        }
    }

    private void setFokusGoodChannel(boolean good, boolean b) {
        if (b) {
            if (good) {
                btnMax.setTextColor(getResources().getColor(R.color.fokus));
                btnMin.setTextColor(getResources().getColor(R.color.noFokus));
            } else {
                btnMax.setTextColor(getResources().getColor(R.color.noFokus));
                btnMin.setTextColor(getResources().getColor(R.color.fokus));
            }
        } else {
            btnMax.setTextColor(getResources().getColor(R.color.noFokus));
            btnMin.setTextColor(getResources().getColor(R.color.noFokus));
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_podelitsa:
                intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, "Приложение 'Ваще Радио', скачивай от сюда - https://play.google.com/store/apps/details?id=com.jonkryl.nache_radio");
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent, "Поделиться"));
                break;
            case R.id.nav_news:
                startActivity(new Intent(this, BrowserActivity.class));
                break;
            case R.id.nav_konfedi:
                startActivity(new Intent(this, KonfendActivity.class));
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void clickAuthor(View view) {
        if (textAuthor.getText().length() > 0) {
            ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("", textAuthor.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, "Скопировано в буфер", Toast.LENGTH_SHORT).show();
        }
    }

    public void getBanner() {
        network.getApiService().getBanner().enqueue(new Callback<ArrayList<BannerDate>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<BannerDate>> call, @NonNull Response<ArrayList<BannerDate>> response) {
                try {
                    int element = random.nextInt(response.body().size());
                    link = response.body().get(element).getLink();
                    linkBanner = response.body().get(element).getLinkBanner();
                    Glide.with(getApplicationContext()).load(linkBanner).into(gifImageView);
                    gifImageView.setOnClickListener(view -> {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                    });
                } catch (Exception ignored) {

                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<BannerDate>> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (event.getDownTime() - lastPressedTime < PERIOD) {
                    stopApp(false);
                } else {
                    Toast.makeText(getApplicationContext(), "Press again to exit.",
                            Toast.LENGTH_SHORT).show();
                    lastPressedTime = event.getEventTime();
                }
                return true;
            }
        }
        return false;
    }
}



package com.jonkryl.nache_radio.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.Dialog.Authorize;
import com.jonkryl.nache_radio.Maket.Message;
import com.jonkryl.nache_radio.R;
import com.jonkryl.nache_radio.Reklama.AppodealBANNERVIEW;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String REQUEST_AUTHORIZE_OK = "requsest_ok";
    private static final int MIN_VISIBLE_SCROOL_POSITION_BUTTON = -3000;
    private LinearLayout inputPanel;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mRef;
    private Button authButton;
    private Authorize authorize;
    private RelativeLayout sendMessage;
    private RecyclerView messageList;
    private LinearLayoutManager mLinearLayoutManager;
    private EditText inputField;
    private LinearLayout scroolDown;
    private AppodealBANNERVIEW appodealBANNERVIEW = new AppodealBANNERVIEW();
    private boolean t = false;

    FirebaseRecyclerAdapter<Message, MsgViewHolder> mRecycleAdapter;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_message_button:
                if (inputField.getText().length() != 0) {
                    mUser = mAuth.getCurrentUser();
                    mRef.child("Chat").push().setValue(new Message(inputField.getText().toString(), mUser.getDisplayName(), mUser.getPhotoUrl().toString(), mUser.getEmail()));
                    inputField.setText("");
                    hideKeyboard();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            messageList.smoothScrollToPosition(mRecycleAdapter.getItemCount() - 1);
                        }
                    }, 500);

                }
                break;
            case R.id.scrool_down_button:
                messageList.smoothScrollToPosition(mRecycleAdapter.getItemCount() - 1);
                scroolDown.setVisibility(View.GONE);
                break;
        }
    }

    public static class MsgViewHolder extends RecyclerView.ViewHolder {
        TextView msg;
        TextView userName;
        CircleImageView userPhoto;
        TextView replyButton;
        TextView timeMessage;
        TextView adminMarker;

        public MsgViewHolder(View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.msg);
            userName = itemView.findViewById(R.id.user_name);
            userPhoto = itemView.findViewById(R.id.avatar);
            replyButton = itemView.findViewById(R.id.reply_button);
            timeMessage = itemView.findViewById(R.id.timeMessage);
            adminMarker = itemView.findViewById(R.id.admin_marker);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        initSource();
        butonClicker();
        if (mUser == null) {
            inputPanelVisible(false);
        } else {
            inputPanelVisible(true);
        }
        authButton.setOnClickListener(view -> authorize.show(getSupportFragmentManager(), authorize.getClass().getName()));

        mRecycleAdapter = new FirebaseRecyclerAdapter<Message, MsgViewHolder>(Message.class, R.layout.item_message, MsgViewHolder.class,
                mRef.child("Chat")) {
            @Override
            protected void populateViewHolder(MsgViewHolder viewHolder, final Message model, final int position) {
                viewHolder.msg.setText(model.getTxtMsg());
                viewHolder.userName.setText(model.getUserName());
                viewHolder.timeMessage.setText(DateFormat.format("dd.MM.yyyy HH:mm", model.getTimeMessage()));
                if (mUser != null) {
                    if (model.getUserName().equals(mUser.getDisplayName())) {
                        viewHolder.replyButton.setVisibility(View.GONE);
                    } else {
                        viewHolder.replyButton.setVisibility(View.VISIBLE);
                        viewHolder.replyButton.setOnClickListener(view -> {
                            inputField.setText(model.getUserName() + ", ");
                            inputField.setSelection(inputField.getText().length());
                            showKeyboard();
                        });
                    }
                } else {
                    viewHolder.replyButton.setVisibility(View.GONE);
                }
                if (model.getUserEmail() != null) {
                    if (model.getUserEmail().equals(Constant.ADMIN_1_EMAIL)) {
                        viewHolder.adminMarker.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.adminMarker.setVisibility(View.GONE);
                    }
                }
                viewHolder.adminMarker.setVisibility(View.GONE);
//                if (viewHolder.userPhoto.toString() == null) {
//                    viewHolder.userPhoto.setImageResource(R.mipmap.main_button);
//                } else {
                    Picasso.with(getApplicationContext())
                            .load(model.getUserPhotoUrl())
                            .into(viewHolder.userPhoto);
//                }
            }
        };

        mRecycleAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int chatMessageCount = mRecycleAdapter.getItemCount();
                int lastVisiblePosition =
                        mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (chatMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    messageList.scrollToPosition(positionStart);
                }
            }
        });
        messageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            int dyAmount;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                dyAmount += dy;
                if (dyAmount < MIN_VISIBLE_SCROOL_POSITION_BUTTON) {
                    if (dy < 0) {
                        scroolDown.setVisibility(View.GONE);
                    }
                    if (dy > 0) {
                        scroolDown.setVisibility(View.VISIBLE);
                    }
                } else {
                    scroolDown.setVisibility(View.GONE);
                }
            }
        });
        messageList.setAdapter(mRecycleAdapter);
        appodealBANNERVIEW.init(this, R.id.appodealBannerViewChat);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (t) {
            t = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        t = false;
    }

    @Override
    protected void onDestroy() {
        getWindow().getDecorView().setBackgroundResource(android.R.color.background_light);
        appodealBANNERVIEW.destroy();
        super.onDestroy();
    }

    public void initSource() {
        scroolDown = findViewById(R.id.scrool_down_button);
        messageList = findViewById(R.id.rv_message_list);
        mLinearLayoutManager = new LinearLayoutManager(this);
        messageList.setLayoutManager(mLinearLayoutManager);
        authorize = new Authorize();
        authButton = findViewById(R.id.auth_button);
        inputPanel = findViewById(R.id.input_panel);
        mAuth = FirebaseAuth.getInstance();
        mRef = FirebaseDatabase.getInstance().getReference();
        mUser = mAuth.getCurrentUser();
        sendMessage = findViewById(R.id.send_message_button);
        inputField = findViewById(R.id.input_field);
        authComplete();
    }

    public void inputPanelVisible(boolean isVisible) {
        if (isVisible) {
            inputPanel.setVisibility(View.VISIBLE);
            authButton.setVisibility(View.GONE);
        } else {
            inputPanel.setVisibility(View.GONE);
            authButton.setVisibility(View.VISIBLE);
        }
    }

    public void authComplete() {
        BroadcastReceiver chatBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                inputPanelVisible(true);
                oneBaseInit();
                messageList.smoothScrollToPosition(mRecycleAdapter.getItemCount() - 1);
            }
        };
        IntentFilter intentFilter = new IntentFilter(Constant.CHAT_BROADCAST);
        registerReceiver(chatBroadcast, intentFilter);
    }

    public void butonClicker() {
        sendMessage.setOnClickListener(this);
        scroolDown.setOnClickListener(this);
    }

    public void oneBaseInit() {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        inputField.clearFocus();
    }

    private void showKeyboard() {
        inputField.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(inputField, InputMethodManager.SHOW_IMPLICIT);
    }

}

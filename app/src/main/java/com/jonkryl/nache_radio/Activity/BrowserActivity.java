package com.jonkryl.nache_radio.Activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.jonkryl.nache_radio.R;
import com.jonkryl.nache_radio.Reklama.AppodealBANNERVIEW;
import com.jonkryl.nache_radio.Reklama.AppodealINTERSTITIAL;
import com.jonkryl.nache_radio.Service.MyWebViewClient;

/**
 * Created by jonkryl on 12.03.2018.
 */

public class BrowserActivity extends AppCompatActivity {
    private WebView mWebView;

    private ProgressBar progressBar;
    private TextView textView;
    private AppodealINTERSTITIAL appodealINTERSTITIAL = new AppodealINTERSTITIAL(this);
    private AppodealBANNERVIEW appodealBANNERVIEW = new AppodealBANNERVIEW();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browser_activity);

        progressBar = findViewById(R.id.pB1);
        textView = findViewById(R.id.tV1);
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            appodealINTERSTITIAL.show(BrowserActivity.this);
            finish();
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mWebView = findViewById(R.id.webView);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.loadUrl("https://www.nashe.ru/news/");
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                    textView.setVisibility(View.VISIBLE);
                }

                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                    textView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        appodealBANNERVIEW.init(this, R.id.appodealBannerViewBrows);
        appodealINTERSTITIAL.init(this);
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            appodealINTERSTITIAL.show(this);
            super.onBackPressed();
        }
    }


    @Override
    protected void onDestroy() {
        appodealINTERSTITIAL.destroy();
        appodealBANNERVIEW.destroy();
        super.onDestroy();
    }
}

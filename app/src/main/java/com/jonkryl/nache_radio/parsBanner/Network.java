package com.jonkryl.nache_radio.parsBanner;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {
    private final String BASE_URL = "https://firebasestorage.googleapis.com/";

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public ApiService.APIService getApiService(){
        return retrofit.create(ApiService.APIService.class);
    }
}

package com.jonkryl.nache_radio.parsBanner;

public class BannerDate {
    private String linkBanner;
    private String link;

    public BannerDate(String linkBanner, String link) {
        this.linkBanner = linkBanner;
        this.link = link;
    }

    public String getLinkBanner() {
        return linkBanner;
    }

    public String getLink() {
        return link;
    }
}

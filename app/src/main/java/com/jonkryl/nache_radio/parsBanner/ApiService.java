package com.jonkryl.nache_radio.parsBanner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    interface APIService {
        @GET("v0/b/vasheradio-e7bab.appspot.com/o/banner.json?alt=media&token=df7740fa-d57c-4e0a-ad12-9d15acff9b4f")
        Call<ArrayList<BannerDate>> getBanner();
    }
}

package com.jonkryl.nache_radio.parsSongInfo;

public class Data {
    private String song, artist;

    public Data(String song, String artist) {
        this.song = song;
        this.artist = artist;
    }

    public String getSong() {
        return song;
    }

    public String getArtist() {
        return artist;
    }
}

package com.jonkryl.nache_radio.parsSongInfo;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {
    private final String BASE_URL = "https://parser.radiovolna.net/";

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public APIService getApiService(){
        return retrofit.create(APIService.class);
    }
}

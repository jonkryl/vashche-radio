package com.jonkryl.nache_radio.parsSongInfo;

import com.jonkryl.nache_radio.Const.Constant;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @FormUrlEncoded
    @POST(Constant.PARSER_URL)
    Call<SongData> getSongDate(@Field("streamId") int streamId,
                                         @Field("streamUrl") String streamUrl,
                                         @Field("nodeParser") int nodeParser);

}

package com.jonkryl.nache_radio.parsSongInfo;

public class SongData {
    private Data data;

    public SongData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }
}

package com.jonkryl.nache_radio.Service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.jonkryl.nache_radio.Const.Constant;
import com.jonkryl.nache_radio.Notification.PlayerNotification;
import com.jonkryl.nache_radio.Player.ExoPlayer;


public class PlayerService extends Service {

    private String streamUrl;

    public boolean nowPlayGlobal = false;
    public boolean goodChannel = false;

    public int nowChannel;

    private PlayerNotification playerNotification;
    private BroadcastReceiver receiver;
    private ExoPlayer exoPlayer;
    private Intent intent;

    private NotificationManager notificationManager;

    public PlayerService() {
    }

    @Override
    public void onCreate() {
        exoPlayer = new ExoPlayer(this);
        playerNotification = new PlayerNotification(this);
        registerReceiver();
        notificationManager = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        unregisterReceiver(receiver);
        notificationManager.cancelAll();
        super.onDestroy();
    }

    private void registerReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                try {
                    switch (action) {
                        case Constant.ACTION_PLAY_PAUSE:
                            playerPlayAndPause(intent.getBooleanExtra(action, false));
                            break;
                        case Constant.ACTION_CLICK_CHANNEL:
                            nowChannel = intent.getIntExtra(action, 1);
                            playerClickChannel(nowChannel);
                            break;
                        case Constant.ACTION_CLICK_GOOD_CHANNEL:
                            goodChannel = intent.getBooleanExtra(action, false);
                            playerClickGoodChannel(goodChannel);
                            break;
                        case Constant.ACTION_REFRESCH_AUTOR:
                            notificationStart(intent.getStringExtra(action));
                            break;
                        case Constant.ACTION_STOP:
                            nowPlayGlobal = false;
                            exoPlayer.closeWGFMPlayer();
                            break;
                        case Constant.ACTION_GET_UPDATE:
                            updateSend();
                            break;
                        case Constant.ACTION_RESTART_PLAY:
                            playerRestartReclama(streamUrl);
                            break;
                        case Constant.ACTION_STOP_PLAYER:
                            stopSelf();
                            break;
                    }
                } catch (Exception ignored) {
                }
            }

        };
        IntentFilter intFilt = new IntentFilter(Constant.ACTION_PLAY_PAUSE);
        intFilt.addAction(Constant.ACTION_CLICK_CHANNEL);
        intFilt.addAction(Constant.ACTION_CLICK_GOOD_CHANNEL);
        intFilt.addAction(Constant.ACTION_REFRESCH_AUTOR);
        intFilt.addAction(Constant.ACTION_STOP);
        intFilt.addAction(Constant.ACTION_GET_UPDATE);
        intFilt.addAction(Constant.ACTION_RESTART_PLAY);
        intFilt.addAction(Constant.ACTION_STOP_PLAYER);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(receiver, intFilt);
    }

    private void updateSend() {
        updateSendBoolean(Constant.ACTION_REFRESCH_INDICATOR, nowPlayGlobal);
        updateSendInt(nowChannel);
        updateSendBoolean(Constant.ACTION_UPDATE_GOOD_CHANNEL, goodChannel);
        updateSendString(exoPlayer.streamAuthor);
    }

    private void updateSendString(String s) {
        intent = new Intent();
        intent.setAction(Constant.ACTION_REFRESCH_AUTOR);
        intent.putExtra(Constant.ACTION_REFRESCH_AUTOR, s);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void updateSendInt(int b) {
        intent = new Intent();
        intent.setAction(Constant.ACTION_UPDATE_NOW_CHANNEL);
        intent.putExtra(Constant.ACTION_UPDATE_NOW_CHANNEL, b);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void updateSendBoolean(String action, boolean b) {
        intent = new Intent();
        intent.setAction(action);
        intent.putExtra(action, b);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void notificationStart(String author) {
        String playPause = "Pause";
        if (author != null) {
            if (author.length() < 2) {
                author = "Тишина";
                playPause = "Play";
            }
            startForeground(Constant.NOTIFY_ID, playerNotification.getNotif(playPause, author, !nowPlayGlobal));
        }
    }

    private void playerClickGoodChannel(boolean goodChannel) {
        selectChannel(nowChannel, goodChannel);
        if (nowPlayGlobal) {
            playerRestart(streamUrl);
        }
    }

    private void playerClickChannel(int nowChannel) {
        selectChannel(nowChannel, goodChannel);
        playerRestart(streamUrl);
    }

    private void playerRestartReclama(String streamUrl) {
        if (streamUrl != null) {
            if (nowPlayGlobal) {
                exoPlayer.stopWGFMPlayer();
                exoPlayer.startWGFMPlayer(streamUrl, nowChannel);
                nowPlayGlobal = true;
            }
        }
    }

    private void playerRestart(String streamUrl) {
        if (streamUrl != null) {
            if (nowPlayGlobal) {
                exoPlayer.stopWGFMPlayer();
                exoPlayer.startWGFMPlayer(streamUrl, nowChannel);
            } else {
                exoPlayer.startWGFMPlayer(streamUrl, nowChannel);
            }
            nowPlayGlobal = true;
        }
    }

    private void playerPlayAndPause(boolean nowPlay) {
        howStreamUrl(nowChannel, goodChannel);
        if (nowPlay) {
            exoPlayer.startWGFMPlayer(streamUrl, nowChannel);
        } else {
            exoPlayer.stopWGFMPlayer();
        }
        nowPlayGlobal = nowPlay;
    }

    private void howStreamUrl(int nowChannel, boolean goodChannel) {
        if (nowChannel == 0) {
            nowChannel = 1;
        }
        selectChannel(nowChannel, goodChannel);
    }

    private void selectChannel(int nowChannel, boolean goodChannel) {
        if (goodChannel) {
            switch (nowChannel) {
                case 1:
                    streamUrl = Constant.NASHE;
                    break;
                case 2:
                    streamUrl = Constant.DVA_NOL;
                    break;
                case 3:
                    streamUrl = Constant.SPB;
                    break;
                case 4:
                    streamUrl = Constant.EKB;
                    break;
                case 5:
                    streamUrl = Constant.CLASSIC;
                    break;
                case 6:
                    streamUrl = Constant.PUNK;
                    break;
                case 7:
                    streamUrl = Constant.POYU;
                    break;
            }
        } else {
            switch (nowChannel) {
                case 1:
                    streamUrl = Constant.NASHE_64;
                    break;
                case 2:
                    streamUrl = Constant.DVA_NOL_64;
                    break;
                case 3:
                    streamUrl = Constant.SPB_64;
                    break;
                case 4:
                    streamUrl = Constant.EKB_64;
                    break;
                case 5:
                    streamUrl = Constant.CLASSIC_64;
                    break;
                case 6:
                    streamUrl = Constant.PUNK_64;
                    break;
                case 7:
                    streamUrl = Constant.POYU_64;
                    break;
            }
        }
    }
}
